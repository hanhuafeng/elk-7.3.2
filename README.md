# ELK-7.3.2集群搭建文档
## 一、ES节点配置选择文档（方便计算机器数量）
### master节点(主节点)

```properties
node.master:true
node.data:false(这里也可以配置成node.data:true)
```

node.master和node.data默认都是true, 但还是建议显式配置

#### 机器配置

普通服务器即可(CPU 内存 消耗一般)

#### 作用

1. 索引的创建或删除
2. 跟踪哪些节点是集群的一部分
3. 决定哪些分片分配给相关的节点

#### 说明

稳定的主节点对集群的健康是非常重要的。
 默认情况下任何一个集群中的节点都有可能被选为主节点。***索引数据和搜索查询等操作会占用大量的cpu，内存，io资源***，为了确保一个集群的稳定，分离主节点和数据节点是一个比较好的选择。虽然主节点也可以协调节点，路由搜索和从客户端新增数据到数据节点，但最好不要使用这些专用的主节点。一个重要的原则是，尽可能做尽量少的工作。
为了防止数据丢失，配置discovery.zen.minimum_master_nodes设置是至关重要的（默认为1），每个主节点应该知道形成一个集群的最小数量的主资格节点的数量。
解释如下：
 假设我们有一个集群。有3个主资格节点，当网络发生故障的时候，有可能其中一个节点不能和其他节点进行通信了。这个时候，当discovery.zen.minimum_master_nodes设置为1的时候，就会分成两个小的独立集群，当网络好的时候，就会出现数据错误或者丢失数据的情况。当discovery.zen.minimum_master_nodes设置为2的时候，一个网络中有两个主资格节点，可以继续工作，另一部分，由于只有一个主资格节点，则不会形成一个独立的集群，这个时候当网络回复的时候，节点又会从新加入集群。
设置这个值的原则是：

> （master_eligible_nodes / 2）+ 1

这个参数也可以动态设置：

```json
PUT _cluster/settings
{
	"transient": {
		"discovery.zen.minimum_master_nodes": 2
	}
}
```

### data节点(数据节点)

#### 配置

```properties
node.master:false(这里也可以配置成node.master:true)
node.data:true
```
#### 机器配置

较高配置服务器, 主要消耗磁盘，内存

#### 作用

1. 存储索引数据
2. 对文档进行增删改查,聚合操作

#### 说明

数据节点对cpu，内存，io要求较高，在优化的时候需要监控数据节点的状态，当资源不够的时候，需要在集群中添加新的节点。
数据节点路径设置,每一个主节点和数据节点都需要知道分片，索引，元数据的物理存储位置，path.data默认位为 $ES_HOME/data，可以通过配置文件 elasticsearch.yml进行修改，例如:

```properties
path.data: /data/es/data/
```

这个设置也可以在命令行上执行，例如：

```bash
./bin/elasticsearch –path.data /data/es/data
```

这个路径最好进行单独配置，这样Elasticsearch的目录和数据的目录就会分开。当删除了Elasticsearch主目录的时候，不会影响到数据。通过rpm安装默认是分开的。
数据目录可以被多个节点共享，甚至可以属于不同的集群，为了防止多个节点共享相同的数据路径，可以在配置文件elasticsearch.yml中添加：

```properties
node.max_local_storage_nodes: 1
```

注意：在相同的数据目录不要运行不同类型的节点（例如：master, data, client）这很容易导致意外的数据丢失。

### client节点(客户端节点/路由节点)

#### 配置

```properties
node.master:false
node.data:false
```

#### 机器配置

普通服务器即可(如果要进行分组聚合操作的话，建议这个节点内存也分配多一点)

#### 作用

1. 处理路由请求

2. 处理搜索

3. 分发索引

#### 说明

从本质上来说该客户节点表现为智能负载平衡器。
独立的客户端节点在一个比较大的集群中是非常有用的，他协调主节点和数据节点，客户端节点加入集群可以得到集群的状态，根据集群的状态可以直接路由请求。
警告：添加太多的客户端节点对集群是一种负担，因为主节点必须等待每一个节点集群状态的更新确认！客户节点的作用不应被夸大，数据节点也可以起到类似的作用。

### 部落节点

#### 配置elasticsearch.yml

```yaml
tribe:
	t1: 
		cluster.name: cluster_one
	t2: 
		cluster.name: cluster_two
```

#### 作用

1. 部落节点可以跨越多个集群，它可以接收每个集群的状态，然后合并成一个全局集群的状态，它可以读写所有节点上的数据

#### 说明

​	T1和T2是任意的名字代表连接到每个集群。
​	上面的示例配置两集群连接，名称分别是T1和T2。默认情况下部落节点通过广播可以做为客户端连接每一个集群。大多数情况下，部落节点可以像单节点一样对集群进行操作。
​	注意：以下操作将和单节点操作不同，如果两个集群的名称相同，部落节点只会连接其中一个。由于没有主节点，当设置local为true的是，主节点的读操作会被自动的执行，例如：集群统计，集群健康度。主节点级别的写操作将被拒绝，这些应该是在一个集群进行。部落	节点可以通过块(block)设置所有的写操作和所有的元数据操作，例如：

```yaml
tribe:
	blocks:
		write: true
		metadata: true
```

部落节点可以也可以在选中的索引块中进行配置，例如：

```yaml
tribe:
	blocks:
		write.indices: hk*,ldn*
		metadata.indices: hk*,ldn*
```

当多个集群有相同的索引名的时候，默认情况下，部落的节点将选择其中一个。这可以通过tribe.on_conflict setting进行配置，可以设置排除那些索引或者指定固定的部落名称。

### Ingest节点(提取节点)

```properties
node.ingest:true
```

#### 作用**

1. Ingest节点和集群中的其他节点一样，但是它能够创建多个处理器管道，用以修改传入文档。类似 最常用的Logstash过滤器已被实现为处理器。

2. Ingest节点 可用于执行常见的数据转换和丰富。 处理器配置为形成管道。 在写入时，Ingest Node有20个内置处理器，例如grok，date，gsub，小写/大写，删除和重命名

3. 在批量请求或索引操作之前，Ingest节点拦截请求，并对文档进行处理。

#### 说明

这样的处理器的一个例子可以是日期处理器，其用于解析字段中的日期。另一个例子是转换处理器，它将字段值转换为目标类型，例如将字符串转换为整数

### 关于节点怎么选择

Elasticsearch的员工 Christian_Dahlqvist解读如下：

```
一个节点的缺省配置是：主节点+数据节点两属性为一身。对于3-5个节点的小集群来讲，通常让所有节点存储数据和具有获得主节点的资格。你可以将任何请求发送给任何节点，并且由于所有节点都具有集群状态的副本，它们知道如何路由请求。

通常只有较大的集群才能开始分离专用主节点、数据节点。 对于许多用户场景，路由节点根本不一定是必需的。

专用协调节点（也称为client节点或路由节点）从数据节点中消除了聚合/查询的请求解析和最终阶段，并允许他们专注于处理数据。 
在多大程度上这对集群有好处将因情况而异。 通常我会说，在查询大量使用情况下路由节点更常见。
```

### 建议

1. 对于Ingest节点，如果我们没有格式转换、类型转换等需求，直接设置为false。
2. 3-5个节点属于轻量级集群，要保证主节点个数满足((节点数/2)+1)。
3. 轻量级集群，节点的多重属性如：Master&Data设置为同一个节点可以理解的。
4. 如果进一步优化，5节点可以将Master和Data再分离。

> 在一个生产集群中我们可以对这些节点的职责进行划分。
> 建议集群中设置3台以上的节点作为master节点【node.master: true node.data: false】
> 这些节点只负责成为主节点，维护整个集群的状态。
>
> 再根据数据量设置一批data节点【node.master: false node.data: true】
> 这些节点只负责存储数据，后期提供建立索引和查询索引的服务，这样的话如果用户请求比较频繁，这些节点的压力也会比较大
>
> 所以在集群中建议再设置一批client节点【node.master: false node.data: false】, 主要是针对海量请求的时候可以进行负载均衡。
> 这些节点只负责处理用户请求，实现请求转发，负载均衡等功能

- 以上内容原文链接：[https://blog.csdn.net/qq_28988969/article/details/92707402](https://blog.csdn.net/qq_28988969/article/details/92707402)

- 以上内容为ES的节点选择

---

## 安装ELK集群的准备步骤（使用cjpt用户）

### 建立elk环境搭建目录

- mkdir /home/cjpt/elk-7.3.2
- cd /home/cjpt/elk-7.3.2
#### 上传三个安装包到此目录下：
- kibana-7.3.2-linux-x86_64.tar.gz
- elasticsearch-7.3.2-linux-x86_64.tar.gz
- logstash-7.3.2.tar.gz
#### ls后得到
```shell
[cjpt@slave01 elk-7.3.2]$ ls  
elasticsearch-7.3.2-linux-x86_64.tar.gz  kibana-7.3.2-linux-x86_64.tar.gz  logstash-7.3.2.tar.gz
```

## 二、ES安装
### 解压ES安装包
- tar -xzvf elasticsearch-7.3.2-linux-x86_64.tar.gz
### 创建数据目录、日志目录
- mkdir /home/cjpt/elk-7.3.2/elasticsearch-7.3.2/data -p
- mkdir /home/cjpt/elk-7.3.2/elasticsearch-7.3.2/logs -p
### 学习elasticsearch.yml配置意义

[elasticsearch.tml](#elasticsearch.yml配置详解)

### 主节点配置
#### 方法一
```cmd
vim /home/cjpt/elk-7.3.2/elasticsearch-7.3.2/config/elasticsearch.yml
```
```yaml
# 集群名称
cluster.name: es
# 节点名称
node.name: es-master
# 存放数据目录,先创建该目录
path.data: /home/app/elk/elasticsearch-7.3.2/data
# 存放日志目录,先创建该目录
path.logs: /home/app/elk/elasticsearch-7.3.2/logs
# 节点IP
network.host: 192.168.43.16
# tcp端口
transport.tcp.port: 9300
# http端口
http.port: 9200
# 种子节点列表，主节点的IP地址必须在seed_hosts中
discovery.seed_hosts: ["192.168.43.16:9300","192.168.43.17:9300","192.168.43.18:9300"]
# 主合格节点列表，若有多个主节点，则主节点进行对应的配置，需要跟【node.name】一致
cluster.initial_master_nodes: ["es-master"]

# 主节点相关配置

# 是否允许作为主节点，以下类型参考【ES节点配置】
node.master: true
# 是否保存数据
node.data: true
node.ingest: false
node.ml: false
cluster.remote.connect: false

# 用来解决跨域，必须写！
http.cors.enabled: true
http.cors.allow-origin: "*"
```
#### 方法二
- 修改安装包目录下的master-config/elasticsearch.yml，然后上传就好了
### 发送给子节点
- scp -r /home/cjpt/elk-7.3.2/elasticsearch-7.3.2 192.168.1.65:/home/cjpt/elk-7.3.2/
- scp -r /home/cjpt/elk-7.3.2/elasticsearch-7.3.2 192.168.1.63:/home/cjpt/elk-7.3.2/
### 修改子节点的配置文件

> 具体的配置文件在安装包下面，修改后直接替换即可

- 替换目录：/home/cjpt/elk-7.3.2/elasticsearch-7.3.2/config
### 根据当前机器的大小修改分配给es的内存
- 文件位置：vim /home/cjpt/elk-7.3.2/elasticsearch-7.3.2/config/jvm.options
> -Xms1g =>修改成想要的内存数  
> -Xmx1g =>修改成想要的内存数
### 添加ik中文分词器（每台都得添加）
- 把elasticsearch-analysis-ik-7.3.2文件夹移动至/home/cjpt/elk-7.3.2/elasticsearch-7.3.2/plugin目录下即可
### 启动es集群
- 此操作需要在各个节点上运行
> /home/cjpt/elk-7.3.2/elasticsearch-7.3.2/bin/elasticsearch -d
### 检测是否启动完成集群
> curl -X GET 'http://192.168.1.64:9200/'  
- 正常启动如下：  
```json
{  
  "name" : "goalive-master",  
  "cluster_name" : "goalive",  
  "cluster_uuid" : "3qSwaKiwQRaXlpGH6s_27w",  
  "version" : {  
    "number" : "7.3.2",  
    "build_flavor" : "default",  
    "build_type" : "tar",  
    "build_hash" : "1c1faf1",  
    "build_date" : "2019-09-06T14:40:30.409026Z",  
    "build_snapshot" : false,  
    "lucene_version" : "8.1.0",  
    "minimum_wire_compatibility_version" : "6.8.0",  
    "minimum_index_compatibility_version" : "6.0.0-beta1"  
  },  
  "tagline" : "You Know, for Search"  
}
```
### 查看启动日志

日志在es运行目录下的logs目录，详细日志在【集群名称】.log下

### 启动遇到问题

- 参考这个网站的解决办法 http://www.voidcn.com/article/p-aqymswln-brv.html
## es-head安装(只需要安装一台)
### es-head使用前需要先安装node！！！
### es-head安装
- 与es同级目录下：
```shell
wget https://github.com/mobz/elasticsearch-head/archive/master.zip
unzip master.zip
cd elasticsearch-head-master
npm install –g grunt–cli
```
- 进入es-head目录
> 因为es-head是一个node项目,所以执行npm install
```shell
npm install
```
- 修改head的连接地址:
1. 找到es-head目录下_site/app.js

2. 修改配置项（藏的比较深，要好好找找）
```shell
vim 【目录】/_site/app.js
```
this.base_uri = this.config.base_uri || this.prefs.get("app-base_uri") || "http://localhost:9200";  
把localhost修改成你es的服务器地址，如:  
this.base_uri = this.config.base_uri || this.prefs.get("app-base_uri") || "http:// 192.168.40.133:9200";

- 启动es-head
> 前台启动：npm run start  
> 后台nohup npm run start &

## 安装kibana(只需要安装一台)
- 此操作只需要在主节点上面操作就可以了

```shell
vim /home/cjpt/elk-7.3.2/kibana-7.3.2-linux-x86_64/config/kibana.yml
```

```yaml
    # 配置kibana的端口
    server.port: 5601
    # 配置监听ip
    server.host: "192.168.43.16"
    # 配置es服务器的ip，如果是集群则配置该集群中主节点的ip,如果有client节点也可以用client节点
    elasticsearch.hosts: "http://192.168.43.16:9200/"
    # 配置kibana的日志文件路径，不然默认是messages里记录日志
    logging.dest:/home/app/elk/kibana-7.3.2-linux-x86_64/logs/kibana.log
```
可以把master-config/kibana.yml进行修改后直接替换
### 启动kibana
mkdir /home/cjpt/elk-7.3.2/kibana-7.3.2-linux-x86_64/logs  
nohup /home/cjpt/elk-7.3.2/kibana-7.3.2-linux-x86_64/bin/kibana > ./logs/kibana.log 2>&1 &

## LogStash安装（按需安装，可以不装）
### 解压安装包
tar -xzvf logstash-7.3.2.tar.gz  
### 修改配置规则
- conf.d/logback_spring.conf根据需要修改  
- 配置好具体的输入输出源后即可启动  
nohup /home/cjpt/elk-7.3.2/alarmLogStash-7.3.2/bin/logstash -f /home/cjpt/elk-7.3.2/alarmLogStash-7.3.2/conf.d/logback_spring.conf > /home/cjpt/elk-7.3.2/alarmLogStash-7.3.2/  logs/alarmLogStash.log 2>&1 &  

### logstash的grok写法

请参考如下网址：https://blog.csdn.net/qq_34021712/article/details/79746413

## elasticsearch.yml配置详解

```yaml
1.1、cluster.name: elasticsearch
配置es的集群名称，默认是elasticsearch，es会自动发现在同一网段下的es，如果在同一网段下有多个集群，就可以用这个属性来区分不同的集群。
1.2、node.name:"Franz Kafka"
节点名，默认随机指定一个name列表中名字，该列表在es的jar包中config文件夹里name.txt文件中，其中有很多作者添加的有趣名字。
1.3、node.master: true
指定该节点是否有资格被选举成为node，默认是true，es是默认集群中的第一台机器为master，如果这台机挂了就会重新选举master。
1.4、node.data: true
指定该节点是否存储索引数据，默认为true。
1.5、index.number_of_shards: 5
设置默认索引分片个数，默认为5片。
1.6、index.number_of_replicas: 1
设置默认索引副本个数，默认为1个副本。
1.7、path.conf: /path/to/conf
设置配置文件的存储路径，默认是es根目录下的config文件夹。
1.8、path.data: /path/to/data
设置索引数据的存储路径，默认是es根目录下的data文件夹，可以设置多个存储路径，用逗号隔开，例：path.data: /path/to/data1,/path/to/data2 
1.9、path.work: /path/to/work
设置临时文件的存储路径，默认是es根目录下的work文件夹。
1.10、path.logs: /path/to/logs
设置日志文件的存储路径，默认是es根目录下的logs文件夹
1.11、path.plugins: /path/to/plugins
设置插件的存放路径，默认是es根目录下的plugins文件夹
1.12、bootstrap.mlockall: true
设置为true来锁住内存。因为当jvm开始swapping时es的效率会降低，所以要保证它不swap，可以把ES_MIN_MEM和 ES_MAX_MEM两个环境变量设置成同一个值，并且保证机器有足够的内存分配给es。同时也要允许elasticsearch的进程可以锁住内存，linux下可以通过ulimit -l unlimited命令。
1.13、network.bind_host: 192.168.0.1
设置绑定的ip地址，可以是ipv4或ipv6的，默认为0.0.0.0。
1.14、network.publish_host: 192.168.0.1
设置其它节点和该节点交互的ip地址，如果不设置它会自动判断，值必须是个真实的ip地址。
1.15、network.host: 192.168.0.1
这个参数是用来同时设置bind_host和publish_host上面两个参数。
1.16、transport.tcp.port: 9300
设置节点间交互的tcp端口，默认是9300。
1.17、transport.tcp.compress: true
设置是否压缩tcp传输时的数据，默认为false，不压缩。
1.18、http.port: 9200
设置对外服务的http端口，默认为9200。
1.19、http.max_content_length: 100mb
设置内容的最大容量，默认100mb
1.20、http.enabled: false
是否使用http协议对外提供服务，默认为true，开启。
1.21、gateway.type: local
gateway的类型，默认为local即为本地文件系统，可以设置为本地文件系统，分布式文件系统，Hadoop的HDFS，和amazon的s3服务器。
1.22、gateway.recover_after_nodes: 1
设置集群中N个节点启动时进行数据恢复，默认为1。
1.23、gateway.recover_after_time: 5m
设置初始化数据恢复进程的超时时间，默认是5分钟。
1.24、gateway.expected_nodes: 2
设置这个集群中节点的数量，默认为2，一旦这N个节点启动，就会立即进行数据恢复。
1.25、cluster.routing.allocation.node_initial_primaries_recoveries: 4
初始化数据恢复时，并发恢复线程的个数，默认为4。
1.26、cluster.routing.allocation.node_concurrent_recoveries: 2
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;添加删除节点或负载均衡时并发恢复线程的个数，默认为4。
1.27、indices.recovery.max_size_per_sec: 0
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;设置数据恢复时限制的带宽，如入100mb，默认为0，即无限制。
1.28、indices.recovery.concurrent_streams: 5
设置这个参数来限制从其它分片恢复数据时最大同时打开并发流的个数，默认为5。
1.29、discovery.zen.minimum_master_nodes: 1
设置这个参数来保证集群中的节点可以知道其它N个有master资格的节点。默认为1，对于大的集群来说，可以设置大一点的值（2-4）
1.30、discovery.zen.ping.timeout: 3s
设置集群中自动发现其它节点时ping连接超时时间，默认为3秒，对于比较差的网络环境可以高点的值来防止自动发现时出错。
1.31、discovery.zen.ping.multicast.enabled: false
设置是否打开多播发现节点，默认是true。
1.32、discovery.zen.ping.unicast.hosts: ["host1", "host2:port", "host3[portX-portY]"]
设置集群中master节点的初始列表，可以通过这些节点来自动发现新加入集群的节点。
下面是一些查询时的慢日志参数设置
index.search.slowlog.level: TRACE
index.search.slowlog.threshold.query.warn: 10s
index.search.slowlog.threshold.query.info: 5s
index.search.slowlog.threshold.query.debug: 2s
index.search.slowlog.threshold.query.trace: 500ms
index.search.slowlog.threshold.fetch.warn: 1s
index.search.slowlog.threshold.fetch.info: 800ms
index.search.slowlog.threshold.fetch.debug:500ms
index.search.slowlog.threshold.fetch.trace: 200ms

```

